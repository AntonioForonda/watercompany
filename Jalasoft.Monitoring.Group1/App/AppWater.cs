﻿using Jalasoft.Monitoring.Group1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Monitoring.Group1.App
{
    internal class AppWater
    {
        readonly int MINIM_ID = 1000;
        private List<Meter> meterList = new List<Meter>();
        public AppWater()
        {
            meterList.Add(new Meter(1000));
        }

        private void CreaterMeter(int id)
        {
            if (id < MINIM_ID)
            {
                Console.WriteLine("The id must be greater or equal than 1000.");
            }
            else if (meterList.FirstOrDefault((meter) => meter.Id == id) != default)
            {
                Console.WriteLine("The id is already taken, enter a diferent one.");
            }
            else 
            { 
                meterList.Add(new Meter(id));
                Console.WriteLine("The meter was registered successfully.");
            }
        }

        public void PrintMenu()
        {
            Console.WriteLine(@"1. Create a new meter.");
            int option = 0;
            if (int.TryParse(Console.ReadLine(), out int number))
            {
                option = number;
            }

            switch (option)
            {
                case 1:
                    Console.WriteLine("Enter the id for a new meter.");
                    int newMeterId = 0;
                    if (int.TryParse(Console.ReadLine(), out int numberId)
                    { 
                        newMeterId = numberId;
                    }
                    this.CreaterMeter(newMeterId);
                    break;
                default:
                    break;
            }
        }
    }
}
