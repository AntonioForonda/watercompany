﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Monitoring.Group1.Models
{
    internal class Meter
    {
        public Meter(int id)
        {
            Id = id;
            RegisterList = new List<Register>();
        }

        public int Id { get; set; }

        public List<Register> RegisterList { get; set; }
    }
}
