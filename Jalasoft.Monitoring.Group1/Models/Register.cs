﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Monitoring.Group1.Models
{
    internal class Register
    {
        public Register(int amount, DateTime registerDate)
        {
            Amount = amount;
            RegisterDate = registerDate;
        }

        public int Amount { get; set; }

        public DateTime RegisterDate { get; set; }
    }
}
